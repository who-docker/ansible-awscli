# Base Ansible Image
FROM willhallonline/ansible

# We need awscli
RUN pip --no-cache-dir install awscli
